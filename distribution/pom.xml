<?xml version="1.0" encoding="UTF-8"?>

<!--
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>org.xwiki.contrib</groupId>
    <artifactId>parent-platform-distribution</artifactId>
    <version>16.10.3</version>
  </parent>
  <groupId>com.xwiki.projects.swp</groupId>
  <artifactId>xwiki-swp-distribution</artifactId>
  <version>0.24-SNAPSHOT</version>
  <packaging>pom</packaging>
  <name>XWiki Projects - Sovereign Workplace Project - Distribution</name>
  <properties>
    <!-- Flavors known to be valid with the current distribution -->
    <xwiki.extension.knownFlavors>
      <!-- Generic flavor for subwikis, in case users want to create a simple new one.  -->
      org.xwiki.platform:xwiki-platform-distribution-flavor-wiki,
      <!-- Main wiki flavors -->
      com.xwiki.projects.swp:xwiki-swp-flavor-community-main/${project.version},
      com.xwiki.projects.swp:xwiki-swp-flavor-enterprise-main/${project.version},
      <!-- Sub-wiki flavor -->
      com.xwiki.projects.swp:xwiki-swp-flavor-community-sub/${project.version},
      com.xwiki.projects.swp:xwiki-swp-flavor-enterprise-sub/${project.version}
    </xwiki.extension.knownFlavors>

    <xwiki.extension.knownInvalidFlavors>
      <!-- Exclude what is usually the default main wiki UI with XWiki Standard -->
      org.xwiki.platform:xwiki-platform-distribution-flavor-mainwiki,
      <!-- Exclude legacy flavors -->
      org.xwiki.enterprise:xwiki-enterprise-ui-mainwiki,
      org.xwiki.enterprise:xwiki-enterprise-ui-wiki,
      org.xwiki.platform:xwiki-platform-distribution-flavor,
      org.xwiki.manager:xwiki-manager-ui,
      org.xwiki.manager:xwiki-manager-wiki-administrator,
      org.xwiki.manager:xwiki-enterprise-manager-wiki-administrator,
      com.xpn.xwiki.products:xwiki-enterprise-manager-wiki-administrator,
      com.xpn.xwiki.products:xwiki-enterprise-wiki,
      <!-- Exclude flavors that are not declared as part of the project anymore -->
      com.xwiki.projects.swp:xwiki-swp-flavor-mainwiki,
      com.xwiki.projects.swp:xwiki-swp-flavor-wiki
    </xwiki.extension.knownInvalidFlavors>

    <!-- Inject extra XWiki Configuration properties -->
    <xwiki.cfg.additionalproperties>
xwiki.authentication.group.allgroupimplicit=1
    </xwiki.cfg.additionalproperties>

    <!-- Inject the distribution parameters as well as the extension repositories in the xwiki properties -->
    <xwiki.properties.additionalproperties>
# Currently, XWiki doesn't come with any support for an office server in openDesk
# however, it is possible in the future to enable support for remote office server with
# Collabora. For now, we will disable office server support.
openoffice.serverType=0
openoffice.autoStart=false

# Make sure to run the distribution wizard automatically for the main wiki
distribution.job.interactive=false
# Also run the distribution wizard automatically for sub-wikis
distribution.job.interactive.wiki=false

# Make sure that we remove double quotes from the system properties
systemPropertiesUpdater.trimDoubleQuotes=true

# Extension manager configuration
# Disable all extension repositories by defeault as openDesk is deployed offline by default
extension.repositories =
# To use remote repositories, uncomment the following lines
# extension.repositories = gitlab-xsas:maven:https://git.xwikisas.com/api/v4/projects/393/packages/maven
# extension.repositories = maven-xwiki:maven:https://nexus.xwiki.org/nexus/content/groups/public
# extension.repositories = store.xwiki.com:xwiki:https://store.xwiki.com/xwiki/rest/
# extension.repositories = extensions.xwiki.org:xwiki:https://extensions.xwiki.org/xwiki/rest/

# Wiki Initializer configuration
wikiInitializer.initializeMainWiki=true
wikiInitializer.initializeSubWikis=true
wikiInitializer.startDistributionWizardOnInit=true

# ********** Workplace services configuration **********
# Define the endpoint where the workplace services should be obtained
# workplaceServices.navigationEndpoint=
# Define the base URL of the workplace
# workplaceServices.base=
# Define the secret to be used for authenticating to the navigation endpoint
# workplaceServices.portalSecret=

# Define the size of the workplace services cache
# The default is :
# workplaceServices.cache.maxEntries=1000

# Define the retention period in seconds of a workplace services cache entry
# The default is set to 30 minutes :
# workplaceServices.cache.lifespan=1800
    </xwiki.properties.additionalproperties>

    <!-- Set the default UI for the main wiki -->
    <xwiki.extension.distribution.ui.groupId>${project.groupId}</xwiki.extension.distribution.ui.groupId>
    <xwiki.extension.distribution.ui.artifactId>xwiki-swp-flavor-community-main</xwiki.extension.distribution.ui.artifactId>
    <xwiki.extension.distribution.ui.version>${project.version}</xwiki.extension.distribution.ui.version>
    <xwiki.extension.distribution.ui>${xwiki.extension.distribution.ui.groupId}:${xwiki.extension.distribution.ui.artifactId}</xwiki.extension.distribution.ui>
    <xwiki.extension.distribution.ui.id>${xwiki.extension.distribution.ui}/${xwiki.extension.distribution.ui.version}</xwiki.extension.distribution.ui.id>

    <!-- Set the default UI for the sub-wikis -->
    <xwiki.extension.distribution.wikiui.groupId>${project.groupId}</xwiki.extension.distribution.wikiui.groupId>
    <xwiki.extension.distribution.wikiui.artifactId>xwiki-swp-flavor-community-sub</xwiki.extension.distribution.wikiui.artifactId>
    <xwiki.extension.distribution.wikiui.version>${project.version}</xwiki.extension.distribution.wikiui.version>
    <xwiki.extension.distribution.wikiui>${xwiki.extension.distribution.wikiui.groupId}:${xwiki.extension.distribution.wikiui.artifactId}</xwiki.extension.distribution.wikiui>
    <xwiki.extension.distribution.wikiui.id>${xwiki.extension.distribution.wikiui}/${xwiki.extension.distribution.wikiui.version}</xwiki.extension.distribution.wikiui.id>

    <!-- ********** Release definitions ********** -->
    <!-- Disable release staging and directly deploy on the configured distributionManagement -->
    <xwiki.nexus.skipLocalStaging>true</xwiki.nexus.skipLocalStaging>
    <xwiki.nexus.skipStaging>true</xwiki.nexus.skipStaging>

    <!-- ********** Plugin versions ********** -->
    <jib.plugin.version>3.4.0</jib.plugin.version>
  </properties>
  <repositories>
    <repository>
      <id>git.xwikisas.com</id>
      <url>https://git.xwikisas.com/api/v4/projects/393/packages/maven</url>
    </repository>
  </repositories>
  <distributionManagement>
    <repository>
      <id>git.xwikisas.com</id>
      <url>https://git.xwikisas.com/api/v4/projects/393/packages/maven</url>
    </repository>
    <snapshotRepository>
      <id>git.xwikisas.com</id>
      <url>https://git.xwikisas.com/api/v4/projects/393/packages/maven</url>
    </snapshotRepository>
  </distributionManagement>
  <developers>
    <developer>
      <id>xwikisas</id>
      <name>XWiki SAS</name>
    </developer>
  </developers>
  <organization>
    <name>XWiki SAS</name>
    <url>https://xwiki.com/</url>
  </organization>
  <build>
    <plugins>
      <plugin>
        <groupId>org.xwiki.contrib</groupId>
        <artifactId>managed-dependencies-maven-plugin</artifactId>
        <version>1.0</version>
        <executions>
          <execution>
            <goals>
              <goal>inject-managed-dependencies</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <!-- The configuration of the enforcer plugin will be inherited from parent POMs, but we need to refer
      to this plugin here in order to make sure that the inject-managed-dependencies mojo is executed before
      the enforcer -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
      </plugin>
    </plugins>
    <pluginManagement>
      <plugins>
        <!--
          Make sure that the WagonTransporter will be used instead of the HttpTransporter when uploading the artifacts.
          We do this because the HttpTransporter doesn't use the <configuration> provided as part of the <server> defined in the Maven settings.
          As such, the HttpTransporter would not send the proper HTTP Headers for authentication against the GitLab Registry.
        -->
        <plugin>
          <artifactId>maven-release-plugin</artifactId>
          <configuration>
            <arguments>-Daether.priority.org.eclipse.aether.transport.wagon.WagonTransporterFactory=100.0</arguments>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
  <modules>
    <module>jetty-hsqldb</module>
    <module>xip</module>
    <module>war</module>
    <module>docker</module>
  </modules>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.xwiki.licensing</groupId>
        <artifactId>application-licensing-licensor-api</artifactId>
        <version>1.27</version>
      </dependency>
      <dependency>
        <groupId>com.xwiki.licensing</groupId>
        <artifactId>application-licensing-common-api</artifactId>
        <version>1.27</version>
      </dependency>
    </dependencies>
  </dependencyManagement>
</project>
