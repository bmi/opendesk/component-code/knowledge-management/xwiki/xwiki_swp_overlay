# Sovereign Workplace

This repository contains the build configuration and the overlay used for integrating XWiki within the [Souveräner Arbeitsplatz](https://www.cio.bund.de/Webs/CIO/DE/digitale-loesungen/digitale-souveraenitaet/souveraener-arbeitsplatz/souverarner-arbeitsplatz-node.html).

![](docs/images/xwiki-components-diagram.png)

The content of this repository are organized as follows : 
* `distribution`: modules used for the distribution of XWiki. It includes the build configuration for the docker images used in the project, as well as the build configuration for a custom XWiki WAR.
* `extension`: modules used as XWiki extensions specific to the Sovereign Workplace Project. These modules are usually present to integrate with the other components of the sovereign workplace and are not generic enough to be released as XWiki Contrib extensions.
* `flavor`: modules providing a default flavor that aggregates the XWiki extensions needed for the main wiki and the sub-wikis of XWiki instances deployed in the Sovereign Workplace.
* `jetty-xwiki`: base docker image with jetty and xwiki data directory to be used on create distribution 

Overview of how this image is used on k8s cluster: 

![](docs/images/xwiki-container-overview.png)

## Features

* When the container starts, the Distribution Wizard is executed and all upgrades and install procedures are executed by default. 
* It's possible to customize page properties using system properties at the container start. This enables to customize XWiki Preferences to use custom layout, language and other settings.  
* This XWiki instance needs to run as a container to be executed on Kubernetes and needs to be managed using Helm. 
* Container image used by this XWiki must use the minimal dependency possible. Ex.: LibreOffice install presented on Official XWiki image is not presented on this image. 
* Container image used by This XWiki has to run with no privileged user and contents from image must be read-only as much is possible. Today, a few files have to be changed to change some preferences/settings during boot. 

## Base Image

Since XWiki supports only Tomcat and Jetty as servlet containers, there are few alternatives to use as base image. The slim/alpine versions are not been maintained anymore. 

### Comparison of supported Servlet containers

|                  | tomcat:9-jre11 | jetty:10-jre11 | jetty:10-jre11-alpine |
|------------------|----------------|----------------|-----------------------|
| OS               | Ubuntu 22.04   | Ubuntu 22.04   | Alpine 3.18.4         |
| Size             | 98.21 MB       | 96.89 MB       | 70.39 MB              |
| Packages         | 219            | 265            | 169                   |
| Vulnerabilities* | 0H 3M 14L      | 0H 3M 14L      | 0H 0M 0L              |
| Rootless         | No             | Yes            | Yes                   |

*Vulnerabilities checked on October/2023. ([tomcat:9-jre11](https://hub.docker.com/layers/library/tomcat/9-jre11/images/sha256-9f8056eed6ac10582079048c8290a4a2d8275fd52d24a3445b07713e39e94f8b?context=explore), 
[jetty:10-jre11](https://hub.docker.com/layers/library/jetty/10-jre11/images/sha256-0cf5c3727e995a07e2cb6bfc3415c999038ef07ac845bc347086f222e73456e7?context=explore), 
[jetty:10-jre11-alpine](https://hub.docker.com/layers/library/jetty/10-jre11-alpine/images/sha256-a4bfabc318a96bf64357c0b8f0ed5ffe1c18c6b89ccd1695081cdbffba05c134?context=explore))

Since tomcat is [using](https://github.com/docker-library/tomcat/issues/209) root user, best choise is use as base image Jetty10 Alpine image. 

## Build

Pre-requirements for building: 
* Java SDK 11 (Temurin is recomended)
* Apache Maven 3.9+
* Docker (to build base image or local images)

### Jetty Base Image

    JETTY_TAG=10-jre11-alpine
    docker build --build-arg="JETTY_TAG=$JETTY_TAG" -t localhost/jetty-xwiki:$JETTY_TAG jetty-xwiki

### XWiki Distributions

    # First compile/build all extensions and base distributions. 
    mvn clean install 

    JETTY_TAG=10-jre11-alpine

    # Generate all image flavors
    mvn package -Pbuild-image -Djetty-xwiki.tag=$JETTY_TAG 
    
    # Generate specific image using local Docker 
    mvn -f distribution/docker/mariadb/pom.xml package jib:dockerBuild \
        -Djib.from.image=localhost/jetty-xwiki:$JETTY_TAG \
        -Djib.to.image=localhost/xwiki:VERSION-mariadb 

## SBOM

Current tool used to generate SBOM is [Syft](https://github.com/anchore/syft). 

    # Generate "more complete" sbom that can be converted to other formats
    syft git.xwikisas.com:5050/xwikisas/swp/xwiki:11-mariadb-jetty-alpine -o syft-json=mariadb.syft.sbom.json
    # Generate CycloneDX SBOM
    syft convert mysql.syft.sbom.json -o cyclonedx-json=mysql-cyclonedx.sbom.json
    # Generate SPDX SBOM
    syft convert mysql.syft.sbom.json -o spdx-json=mysql-spdx.sbom.json

## Scanning

Current tool used to run security scanner on this project container images is [Trivy](https://github.com/aquasecurity/trivy). 

    trivy image git.xwikisas.com:5050/xwikisas/swp/xwiki:11-mariadb-jetty-alpine

## Container Signature

Current tool used to sign containers is [cosign](https://github.com/sigstore/cosign), tool that is part of (sigstore)(https://www.sigstore.dev/) project. 

### Sign 

Using Gitlab ingration to read keys from Project. This require developer access to this project. 

    GITLAB_HOST=https://git.xwikisas.com/
    # https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
    GITLAB_TOKEN=generatedPersonalToken 
    # Sign image 
    cosign sign --key gitlab://393 git.xwikisas.com:5050/xwikisas/swp/xwiki:IMAGE_TAG@sha256:digest
    # Add sbom to container (not sign yet)
    cosign attach sbom --type=spdx --input-format=json --sbom=mariadb-spdx.sbom.json git.xwikisas.com:5050/xwikisas/swp/xwiki:IMAGE_TAG@sha256:digest 
    # Sign sbom 
    cosign sign --key gitlab://393 --attachment sbom git.xwikisas.com:5050/xwikisas/swp/xwiki:IMAGE_TAG@sha256:digest 

### Verifying

All containers images generated by this project can be [verified](https://docs.sigstore.dev/verifying/verify/) with this public key: 

    -----BEGIN PUBLIC KEY-----
    MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEoNMq65mDRUJa1HdoKP2QNr9AN04v
    JwsRwVRbGmw4RtzKTHa3+Kj8BfonP72FqOhUr9WkDYYPPyBLTugCDwj6yw==
    -----END PUBLIC KEY-----

Example check tag version: 0.11-mariadb-jetty-alpine: 

    cosign verify --key cosign.pub git.xwikisas.com:5050/xwikisas/swp/xwiki:0.11-mariadb-jetty-alpine

## General Security Checks

Some checks that can be performed to verify the integrity of this running container (if running on Kubernetes using XWiki Helm Chart): 

* [Check Helm Chart signature](https://extensions.xwiki.org/xwiki/bin/view/Extension/XWikiHelm/#HSigning)
* [Check XWiki (OpenDesk) signature](https://git.xwikisas.com/xwikisas/swp/xwiki#sign)
* Store and check if config files are not changed on running instance
```
    kubectl -n NAMESPACE get pod xwiki-0 -o jsonpath="{.metadata.annotations.checksum/configmap}"
    kubectl -n NAMESPACE get pod xwiki-0 -o jsonpath="{.metadata.annotations.checksum/initialization-configmaps}"
```
* Run frequent security container scans, see [example](https://git.xwikisas.com/xwikisas/swp/xwiki#sign).

