/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices.internal;

import java.util.List;

import com.xwiki.projects.swp.workplaceservices.WorkplaceServiceCategory;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesEntry;

/**
 * Default implementation of {@link WorkplaceServicesEntry}.
 *
 * @version $Id$
 * @since 1.0
 */
public class DefaultWorkplaceServicesCategory implements WorkplaceServiceCategory
{
    private String identifier;

    private String displayName;

    private List<WorkplaceServicesEntry> entries;

    /**
     * Build a new {@link DefaultWorkplaceServicesCategory}.
     *
     * @param identifier the identifier
     * @param displayName the display name
     * @param entries the entries
     */
    public DefaultWorkplaceServicesCategory(String identifier, String displayName, List<WorkplaceServicesEntry> entries)
    {
        this.identifier = identifier;
        this.displayName = displayName;
        this.entries = entries;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public String getDisplayName()
    {
        return displayName;
    }

    @Override
    public List<WorkplaceServicesEntry> getEntries()
    {
        return entries;
    }
}
