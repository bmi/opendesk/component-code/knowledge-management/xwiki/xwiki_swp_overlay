/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices;

import java.net.URL;

import org.xwiki.component.annotation.Role;

/**
 * Configuration for the Workplace Services.
 *
 * @version $Id$
 * @since 1.0
 */
@Role
public interface WorkplaceServicesConfiguration
{
    /**
     * @return the URL of the navigation API endpoint
     * @throws WorkplaceServicesException if an error happens
     */
    URL getNavigationEndpoint() throws WorkplaceServicesException;

    /**
     * @return the base URL used by the workplace services
     * @throws WorkplaceServicesException if an error happens
     */
    URL getBase() throws WorkplaceServicesException;

    /**
     * @return the secret to be used when interacting with the API
     * @throws WorkplaceServicesException if an error happens
     */
    String getPortalSecret() throws WorkplaceServicesException;

    /**
     * @return the maximum amount of entries allowed in the workplace services cache
     * @throws WorkplaceServicesException if an error happens
     */
    int getServiceCacheMaxEntries() throws WorkplaceServicesException;

    /**
     * @return the lifespan of a cache entry in seconds
     * @throws WorkplaceServicesException if an error happens
     */
    int getServiceCacheEntryLifespan() throws WorkplaceServicesException;
}
