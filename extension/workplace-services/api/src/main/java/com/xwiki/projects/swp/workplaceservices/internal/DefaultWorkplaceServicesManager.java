/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices.internal;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.xwiki.component.annotation.Component;
import org.xwiki.model.reference.DocumentReference;

import com.xpn.xwiki.XWikiContext;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServiceCategory;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesCache;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesException;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesManager;

/**
 * Default implementation of {@link WorkplaceServicesManager}.
 *
 * @version $Id$
 * @since 1.0
 */
@Component
@Singleton
public class DefaultWorkplaceServicesManager implements WorkplaceServicesManager
{
    @Inject
    private WorkplaceServicesCache workplaceServicesCache;

    @Inject
    private WorkplaceServicesAPI workplaceServicesAPI;

    @Inject
    private Provider<XWikiContext> contextProvider;

    @Inject
    private Logger logger;

    @Override
    public List<WorkplaceServiceCategory> getCategories(DocumentReference userReference)
        throws WorkplaceServicesException
    {
        // Try first to get the entry from the cache
        Locale locale = getLocale();
        List<WorkplaceServiceCategory> categories = workplaceServicesCache.getCategories(userReference, locale);

        if (categories == null) {
            // Load the categories and store them in cache
            List<WorkplaceServiceCategory> newCategories =
                workplaceServicesAPI.fetchCategories(userReference, locale);

            if (newCategories.isEmpty()) {
                newCategories = getGuestCategories();
            }
            workplaceServicesCache.setCategories(userReference, locale, newCategories);

            return newCategories;
        } else {
            return categories;
        }
    }

    @Override
    public List<WorkplaceServiceCategory> getGuestCategories() throws WorkplaceServicesException
    {
        // Try first to get the entry from the cache
        Locale locale = getLocale();
        List<WorkplaceServiceCategory> categories = workplaceServicesCache.getGuestCategories(locale);

        if (categories == null) {
            // Load the categories and store them in cache
            List<WorkplaceServiceCategory> newCategories = workplaceServicesAPI.fetchCategories(null, locale);
            workplaceServicesCache.setGuestCategories(locale, newCategories);
            return newCategories;
        } else {
            return categories;
        }
    }

    private Locale getLocale()
    {
        XWikiContext context = contextProvider.get();

        logger.debug("Computing locale to use for workplace services ...");
        Locale locale = Locale.getDefault();
        logger.debug("Default locale is : [{}]", locale);
        if (context.getLocale() != null) {
            locale = context.getLocale();
            logger.debug("Updating from #getLocale() with [{}]", locale);
        } else if (context.getInterfaceLocale() != null) {
            locale = context.getInterfaceLocale();
            logger.debug("Updating from #getInterfaceLocale() with [{}]", locale);
        }

        if (locale == null) {
            locale = Locale.ROOT;
            logger.debug("Locale is still null ... updating with [{}]", Locale.ROOT);
        }

        return locale;
    }
}
