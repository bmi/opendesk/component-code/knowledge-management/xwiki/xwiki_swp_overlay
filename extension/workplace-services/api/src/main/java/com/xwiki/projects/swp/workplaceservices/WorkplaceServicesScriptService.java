/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices;

import java.net.URL;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.component.annotation.Component;
import org.xwiki.model.ModelContext;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.script.service.ScriptService;
import org.xwiki.security.authorization.AccessDeniedException;
import org.xwiki.security.authorization.AuthorizationManager;
import org.xwiki.security.authorization.Right;
import org.xwiki.text.StringUtils;

/**
 * Script service for the workplace services.
 *
 * @version $Id$
 * @since 1.0
 */
@Component
@Named(WorkplaceServicesScriptService.SERVICE_NAME)
@Singleton
public class WorkplaceServicesScriptService implements ScriptService
{
    /**
     * The script service name.
     */
    public static final String SERVICE_NAME = "workplaceservices";

    @Inject
    private AuthorizationManager authorizationManager;

    @Inject
    private WorkplaceServicesManager workplaceServicesManager;

    @Inject
    private WorkplaceServicesConfiguration workplaceServicesConfiguration;

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Inject
    private ModelContext modelContext;

    @Inject
    private Logger logger;

    /**
     * Get the categories associated to the current user.
     *
     * @return the available categories
     * @throws WorkplaceServicesException if an error happens
     */
    public List<WorkplaceServiceCategory> getCategories() throws WorkplaceServicesException
    {
        return workplaceServicesManager.getCategories(documentAccessBridge.getCurrentUserReference());
    }

    /**
     * Get the categories associated to the given user.
     *
     * @param userReference the user for which the categories should be fetched
     * @return the available categories
     * @throws WorkplaceServicesException if an error happens
     * @throws AccessDeniedException if the current user is not allowed to call this method
     */
    public List<WorkplaceServiceCategory> getCategories(DocumentReference userReference)
        throws WorkplaceServicesException, AccessDeniedException
    {
        if (!documentAccessBridge.getCurrentUserReference().equals(userReference)) {
            authorizationManager.checkAccess(Right.PROGRAM, documentAccessBridge.getCurrentUserReference(),
                modelContext.getCurrentEntityReference());
        }

        return workplaceServicesManager.getCategories(userReference);
    }

    /**
     * @return true if the workplace services configuration is valid
     */
    public boolean hasValidConfiguration()
    {
        try {
            if (StringUtils.isBlank(workplaceServicesConfiguration.getPortalSecret())) {
                return false;
            } else {
                URL url = workplaceServicesConfiguration.getNavigationEndpoint();
                return !url.toString().isBlank();
            }
        } catch (WorkplaceServicesException e) {
            logger.error("Invalid workplace services configuration", e);
            return false;
        }
    }
}
