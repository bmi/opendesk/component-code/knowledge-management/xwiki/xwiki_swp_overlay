/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices.internal;

import java.net.URL;

import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesEntry;

/**
 * Default implementation of {@link WorkplaceServicesEntry}.
 *
 * @version $Id$
 * @since 1.0
 */
public class DefaultWorkplaceServicesEntry implements WorkplaceServicesEntry
{
    private String identifier;

    private String displayName;

    private URL icon;

    private URL link;

    private String target;

    /**
     * Constructs a new {@link DefaultWorkplaceServicesEntry}.
     *
     * @param identifier the identifier
     * @param displayName the display name
     * @param icon the icon URL
     * @param link the service URL
     * @param target the target
     */
    public DefaultWorkplaceServicesEntry(String identifier, String displayName, URL icon, URL link, String target)
    {
        this.identifier = identifier;
        this.displayName = displayName;
        this.icon = icon;
        this.link = link;
        this.target = target;
    }

    @Override
    public String getIdentifier()
    {
        return identifier;
    }

    @Override
    public String getDisplayName()
    {
        return displayName;
    }

    @Override
    public URL getIcon()
    {
        return icon;
    }

    @Override
    public URL getLink()
    {
        return link;
    }

    @Override
    public String getTarget()
    {
        return target;
    }
}
