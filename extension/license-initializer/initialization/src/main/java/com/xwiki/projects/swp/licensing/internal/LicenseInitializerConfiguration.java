/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.licensing.internal;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.xwiki.component.annotation.Component;
import org.xwiki.instance.InstanceId;

import com.xwiki.licensing.License;

/**
 * Configuration for the license initializer.
 *
 * @version $Id$
 * @since 0.21
 */
@Component(roles = LicenseInitializerConfiguration.class)
@Singleton
public class LicenseInitializerConfiguration
{
    @Inject
    private Logger logger;

    private InstanceId instanceId;

    private List<License> licenses = Collections.emptyList();

    /**
     * @return the instance ID found from licenses to XWiki using system properties
     */
    public InstanceId getInferredInstanceId()
    {
        return instanceId;
    }

    /**
     * @return the list of licenses passed to XWiki using system properties
     */
    public List<License> getLicenses()
    {
        return licenses;
    }

    /**
     * @param licences the licenses to use
     */
    public void setLicences(List<License> licences)
    {
        // From the set of licenses received, infer the instance ID based on the first instance ID found in the first
        // license of the list.
        if (!licences.isEmpty()) {
            Collection<InstanceId> applicableInstanceIds = licences.get(0).getInstanceIds();
            if (!applicableInstanceIds.isEmpty()) {
                this.instanceId = applicableInstanceIds.iterator().next();
                logger.info("Inferring instance ID as [{}]", instanceId);
            }
        }

        this.licenses = licences;
    }
}
