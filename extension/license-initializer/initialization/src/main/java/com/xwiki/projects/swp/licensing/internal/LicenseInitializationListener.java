/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.licensing.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.xwiki.component.annotation.Component;
import org.xwiki.crypto.BinaryStringEncoder;
import org.xwiki.observation.AbstractEventListener;
import org.xwiki.observation.event.ApplicationStartedEvent;
import org.xwiki.observation.event.Event;
import org.xwiki.properties.converter.Converter;

import com.xwiki.licensing.License;

/**
 * Listener that will load licenses form the JVM properties when XWiki is starting-up.
 * We need to call this listener very early in the start-up process in order to define an inferred XWiki instance ID
 * from the license we parse.
 *
 * @version $Id$
 * @since 0.21
 */
@Component
@Singleton
@Named(LicenseInitializationListener.LISTENER_NAME)
public class LicenseInitializationListener extends AbstractEventListener
{
    /**
     * The listener name.
     */
    public static final String LISTENER_NAME = "licenseInitializationListener";

    @Inject
    @Named("Base64")
    private BinaryStringEncoder base64Decoder;

    @Inject
    private Converter<License> licenseConverter;

    @Inject
    private LicenseInitializerConfiguration licenseInitializerConfiguration;

    @Inject
    private Logger logger;

    /**
     * The default constructor.
     */
    public LicenseInitializationListener()
    {
        super(LicenseInitializationListener.LISTENER_NAME, new ApplicationStartedEvent());
    }

    @Override
    public void onEvent(Event event, Object source, Object data)
    {
        String[] rawLicenses = System.getProperties().getProperty("licenses", StringUtils.EMPTY).split(",");

        List<License> licenses = new ArrayList<>();

        for (String rawLicense : rawLicenses) {
            if (StringUtils.isNotBlank(rawLicense)) {
                try {
                    License license = licenseConverter.convert(License.class, base64Decoder.decode(rawLicense));
                    logger.info("Found license [{}] in system properties", license);
                    licenses.add(license);
                } catch (IOException e) {
                    logger.error("Failed to decode license for initialization", e);
                }
            } else {
                logger.debug("No license found to be initialized.");
            }
        }

        licenseInitializerConfiguration.setLicences(licenses);
    }
}
