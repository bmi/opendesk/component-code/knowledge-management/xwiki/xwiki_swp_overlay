/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.auth.internal;

import java.security.Principal;

import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xwiki.classloader.ClassLoaderManager;
import org.xwiki.component.annotation.Component;
import org.xwiki.contrib.oidc.auth.OIDCAuthServiceImpl;
import org.xwiki.security.authservice.XWikiAuthServiceComponent;

import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;
import com.xpn.xwiki.user.api.XWikiAuthService;
import com.xpn.xwiki.user.api.XWikiUser;
import com.xpn.xwiki.web.Utils;

/**
 * Bridge used for checking HTTP Bearer Access tokens against Keycloak.
 *
 * @version $Id$
 * @since 0.23
 */
@Component
@Singleton
@Named(KeycloakBridgeAuth.ID)
public class KeycloakBridgeAuth implements XWikiAuthService, XWikiAuthServiceComponent
{
    /**
     * The authentication service ID.
     */
    public static final String ID = "keycloak-bridge-auth";

    private static final String AUTH_SERVICE = "org.xwiki.contrib.oidc.auth.OIDCAuthServiceImpl";

    private static final Logger LOGGER = LoggerFactory.getLogger(KeycloakBridgeAuth.class);

    private KeycloakAuthService keycloakAuthService;

    private XWikiAuthService authService;

    /**
     * Default constructor.
     */
    public KeycloakBridgeAuth()
    {
        this.keycloakAuthService = Utils.getComponent(KeycloakAuthService.class);

        LOGGER.debug("Using custom AuthClass [{}].", AUTH_SERVICE);

        try {
            // Get the current ClassLoader
            @SuppressWarnings("deprecation")
            ClassLoaderManager clManager = Utils.getComponent(ClassLoaderManager.class);
            ClassLoader classloader = null;
            if (clManager != null) {
                classloader = clManager.getURLClassLoader("wiki:xwiki", false);
            }

            // Get the class
            if (classloader != null) {
                this.authService = (XWikiAuthService) Class.forName(AUTH_SERVICE, true, classloader).newInstance();
            } else {
                this.authService = (XWikiAuthService) Class.forName(AUTH_SERVICE).newInstance();
            }

            LOGGER.debug("Initialized AuthService using Reflection.");

            return;
        } catch (Exception e) {
            LOGGER.warn("Failed to initialize AuthService [{}] using Reflection, trying with 'new'.", AUTH_SERVICE, e);
        }

        this.authService = new OIDCAuthServiceImpl();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Initialized AuthService [{}] using 'new'.", this.authService.getClass().getName());
        }
    }

    @Override
    public XWikiUser checkAuth(XWikiContext context) throws XWikiException
    {
        String authorization = context.getRequest().getHeader("Authorization");
        LOGGER.debug("Authorization header [{}].", authorization);

        try {
            XWikiUser user = this.keycloakAuthService.checkAuth(authorization);

            if (user != null) {
                return user;
            }
        } catch (Exception e) {
            LOGGER.debug("Failed to get OIDC user from HTTP authorization [" + authorization + "]", e);
        }

        return this.authService.checkAuth(context);
    }

    @Override
    public XWikiUser checkAuth(String username, String password, String rememberme, XWikiContext context)
        throws XWikiException
    {
        return authService.checkAuth(username, password, rememberme, context);
    }

    @Override
    public void showLogin(XWikiContext context) throws XWikiException
    {
        authService.showLogin(context);
    }

    @Override
    public Principal authenticate(String username, String password, XWikiContext context) throws XWikiException
    {
        return authService.authenticate(username, password, context);
    }

    @Override
    public String getId()
    {
        return ID;
    }
}
